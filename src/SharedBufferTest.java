// SharedBufferTest cria threads produtor e consumidor

public class SharedBufferTest {
    public static void main(String[] args){
        // cria objeto compartilhado usado por threads 
        Buffer sharedLocation = new UnsynchronizedBuffer();
        
        // cria objetos produtor e consumidor 
        Producer producer = new Producer(sharedLocation);
        Consumer consumer = new Consumer(sharedLocation);
        
        producer.start(); // inicia thread produtor 
        consumer.start(); // inicia thread consumidor 
    }
}