// Método run do Consumer controla uma thread que executa um laço
// quatro vezes e lê um valor de sharedLocation cada vez
public class Consumer extends Thread {
    private Buffer sharedLocation; // referência do objeto compartilhado 
    
    // Construtor Consumer
    public Consumer(Buffer shared){
        super("Consumidor"); // Crie a thread chamada "Consumer"
        sharedLocation = shared; // Inicializa sharedLocation
    }

    // le o valor de sharedLocation quatro vezes e soma os valores
    public void run(){
        int sum = 0;
        
        // alterna entre valor sleeping e getting Buffer 
        for (int count = 0; count <= 20; ++count){
            // dorme 0-3 segundos, le o valor de Buffer e adiciona ao total 
            try {
                Thread.sleep((int) (Math.random() * 3001)); 

                sum += sharedLocation.get();
            }
            // se thread adormecida interrompida, imprime cópia da pilha
            catch (InterruptedException exception){
                exception.printStackTrace();
            }
        }
        
        System.err.println(getName() + " lê valores totalizando: "
                + sum + ".\nTerminando "+getName()+".");        
    }    
}