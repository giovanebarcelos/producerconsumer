// Método run de Producer controla uma thread produto que 
// armazena valores de 1 a 4 em Buffer sharedLocation

public class Producer extends Thread {
    private Buffer sharedLocation; // referência a objetivo compartilhado 
    
    // construtor Producer 
    public Producer(Buffer shared){
        super("Produtor"); // cria thread chamada de "Producer"
        sharedLocation = shared; // inicializa sharedLocation 
    }    
    
    // método run de Producer armazena valores de 
    // 1 a 4 no Buffer sharedLoccation 
    public void run(){
        for (int count = 1; count <= 20; count++){
            // dorme de 0 a 3 segundos, então coloca valor em Buffer 
            try {
                Thread.sleep((int) (Math.random() * 3001));
                sharedLocation.set(count); // esreve para o buffer 
            } 
            // se thread adormecida for interrompida, imprime cópia da pilha
            catch (InterruptedException exception){
                exception.printStackTrace();
            }
        }
        
        System.err.println(getName() + " terminou produção" + 
                "\nTerminando "+getName()+".");
    }
}
