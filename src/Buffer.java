// Interface Buffer especifica métodos para acessar dados do buffer

public interface Buffer {
    public void set(int value); // coloca valor em Buffer
    public int get(); // retorna valor do Buffer    
}