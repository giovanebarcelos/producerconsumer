// UnsynchronizedBuffer mantém o inteiro compartilhado que é acessado
// por uma thread produtora e uma thread consumidoravia métodos set e get 

public class UnsynchronizedBuffer implements Buffer {
    private int buffer = -1; // compartilhado por Producer e Consumer 
    
    // coloque valor no buffer 
    public void set(int value){
        System.err.println(Thread.currentThread().getName() + 
                " setou " + value);
        
        buffer = value;
    }
    
    // retorna o valor do buffer 
    public int get(){
        System.err.println(Thread.currentThread().getName() + 
                " retornou " + buffer);
        
        return buffer;
    }
}